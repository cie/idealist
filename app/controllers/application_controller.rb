class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  check_authorization(:unless=>:devise_controller?)

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  def unauthorized
    redirect_to "/403.html"
  end 

  def not_found
    redirect_to "/404.html"
  end 

end
