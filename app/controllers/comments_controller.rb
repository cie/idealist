class CommentsController < ApplicationController

  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @idea, notice: 'Idea was successfully created.' }
        format.json { render action: 'show', status: :created, location: @idea }
      else
        format.html { render action: 'new' }
        format.json { render json: @idea.errors, status: :unprocessable_entity }
      end
    end 
  end 

  def update
    @comment.update()
  end

  def destroy
    @comment.destroy
    respond_to
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end 
  
  def comment_params
    params.require(:comment).permit(:text, :subject_type, :subject_id)
  end 
end
