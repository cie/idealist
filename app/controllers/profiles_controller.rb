class ProfilesController < ApplicationController
  load_and_authorize_resource class: User
  before_action { @user = @profile; @users = @profiles }

  def index
  end

  def show
  end

  def update
    respond_to do |format|
      if @user.update params[:user].except(:email, :password, :current_password, :password_confirmation)
        format.html { redirect_to profile_path @user }
      else
        # XXX Does not work. Cannot render other controller's views
        # because it relies on helpers. Should use widgets
        # Now no problem because no validations on profile fields.
        format.html { render 'devise/registrations/edit' }
      end
    end 
  end

  private

end
