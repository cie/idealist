class WelcomeController < ApplicationController
  def index
    authorize! :show, Idea
    number_of_ideas = 3
    @ideas = Idea.accessible_by(Ability.new, :read).all[0...number_of_ideas]
  end
end
