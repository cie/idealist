module MarkdownHelper
  def markdown string
    GitHub::Markdown.render_gfm(string || "").html_safe
  end 
end 
