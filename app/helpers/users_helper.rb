module UsersHelper

  def user_link user, clazz="user-name"
    bgclass = user.dark_bg? ? "dark-bg" : "light-bg"
    "<a href='#{profile_path(user)}' class='#{clazz} #{bgclass}' >".html_safe + user_name(user, "") + "</a>".html_safe
  end 

  def user_name user, clazz="user-name"
    bgclass = user.dark_bg? ? "dark-bg" : "light-bg"
    "<span class='#{clazz} #{bgclass}' style='color: #{user.color}'>".html_safe +
      user.display_name + "</span>".html_safe
  end 
end 
