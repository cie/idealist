class Ability
  include CanCan::Ability

  def initialize(user=nil)
    # verbs we use
    # read: can index, show
    # create: can create, new
    # update: can edit, update
    # destroy: can destroy
    # manage: can do anything

    can :read, Idea
    can :read, Comment

    return unless user

    can :read, User
    can :manage, User, id:user.id

    can :create, Idea
    can :update, Idea

    can :create, Comment
    can :manage, Comment, user_id: user.id
  end
end
