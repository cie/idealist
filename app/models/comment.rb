class Comment
  include Mongoid::Document

  field :text, type: String
  validates :text, length: 10..1000

  belongs_to :user
  validates :user, presence: true

  belongs_to :subject, polymorphic: true
  validates :subject, presence: true
end
