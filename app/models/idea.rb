class Idea
  include Mongoid::Document
  include Mongoid::Slug

  belongs_to :user
  validates :user, :presence=>true


  field :title, type: String
  validates :title, :presence=>true
  slug :title

  # This field has been given name by two mistakes. It should be called 'tags'.
  field :tagline, type: String
  validates :tagline, :presence=>true, :length=>10..160

  field :description, type: String
  validates :description, :presence=>true, :length=>10..3000

  has_many :comments, as: :subject

end
