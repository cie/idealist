class User
  include Mongoid::Document
  include ActiveModel::MassAssignmentSecurity
  include Mongoid::Slug

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable,
         :confirmable, :omniauthable

  attr_accessible :email, :password, :password_confirmation

  field :name,              :type => String, default: ""
  def display_name
    return name unless name.blank?
    email =~ /\A(.*?)@/
    $1
  end 
  attr_accessible :name
  validates :name, length: { within: 0..60, if: -> {not name.blank?} }
  slug :name

  field :about_me,          :type => String
  validates :about_me, length: { within: 0...400, if: -> {not about_me.blank?} }
  attr_accessible :about_me


  COLOR_FORMAT = /\A#[0-9a-f]{6}\z/
  DEFAULT_COLOR = "#ffffff"
  field :color,             :type => String, default: DEFAULT_COLOR
  validates :color, :presence => true, :format => COLOR_FORMAT
  attr_accessible :color
  def dark_bg?
    User.bright?(color)
  end 
  def self.bright? color
    red   = color[1..2].to_i(16)
    green = color[3..4].to_i(16)
    blue  = color[5..6].to_i(16)
    #luma = 0.2126*red + 0.7152*green + 0.0722*blue
    luma = 0.28*red + 0.48*green + 0.23*blue
    luma > 128
  end 



  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time
  #

  field :facebook_token,          type: String
  field :facebook_expires_at,     type: Time
  def self.find_for_facebook_oauth auth, current_user
    if current_user
      user = current_user
    else
      user = where(email: auth['info']['email']).first

      if not user
        user = User.new email: auth['info']['email'], name: auth['info']['name'], confirmed_at: Time.now
      else
        unless user.confirmed_at
          user.email = auth['info']['email']
          user.confirmed_at = Time.now
        end
      end
    end

    user.facebook_token = auth['credentials']['token']
    user.facebook_expires_at = Time.at auth['credentials']['expires_at']

    user.save!

    user
  end 

  def password_required?
    return false if facebook_token && password.nil? && password_confirmation.nil?
    super
  end 

  has_many :comments

end
