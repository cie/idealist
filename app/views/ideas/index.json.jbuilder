json.array!(@ideas) do |idea|
  json.extract! idea, :title, :tagline, :description
  json.url idea_url(idea, format: :json)
end
