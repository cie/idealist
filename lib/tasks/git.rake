
task "deploy" => "push" do
  sh "git push heroku master"
end 

task "push" => "commit" do
  sh "git push origin master"
end 

task "commit" do
  unless `git st` =~ /nothing to commit/
    sh "git add ."
    sh "git commit -a"
  end
end 
