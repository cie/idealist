FactoryGirl.define do

  factory :user do
    name {Faker::Name.name}
    email {Faker::Internet.email}
  end 

  factory :idea do
    title  {Faker::Lorem.words}
    tagline {Faker::Lorem.words}
    description { Faker::Lorem.paragraphs}
  end 
end 
