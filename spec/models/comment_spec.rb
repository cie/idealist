require 'spec_helper'

describe Comment do
  before do
    @user = FactoryGirl.build(:user)
    @idea = FactoryGirl.build(:idea)
  end 

  it "can be created" do
    c = Comment.new user: @user, subject: @idea, text: Faker::Lorem.characters
    c.save!
  end 

end
