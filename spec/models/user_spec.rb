describe User do
  describe ".bright?" do
    specify { User.should     be_bright "#ffffff"}
    specify { User.should     be_bright "#ffff00"}
    specify { User.should     be_bright "#00ffff"}
    specify { User.should     be_bright "#ff55ee"}
    specify { User.should     be_bright "#12ff07"}
    specify { User.should     be_bright "#FF446A"}
    specify { User.should_not be_bright "#d98a00"}
    specify { User.should_not be_bright "#071BFF"}
    specify { User.should_not be_bright "#000C98"}
    specify { User.should_not be_bright "#98001F"}
    specify { User.should_not be_bright "#98001F"}
    specify { User.should_not be_bright "#000000"}
  end
end 
